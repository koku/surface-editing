"""
GRID.PY:
	Contains classes that represent a grid

Authors: Chris Janaqi, Alexandre Frances
Created: 25.11.2019
"""
import numpy as np
from typing import Tuple, Union, Optional


class Grid:
	def __init__(self, xlim: np.ndarray, ylim: np.ndarray, num: np.ndarray):
		"""
		:param xlim: np.ndarray containing xmin & xmax.
		:param ylim: np.ndarray containing ymin & ymax.
		:param num: np.ndarray containing resolution along x and y.
		"""
		assert num[0] >= 2 and num[1] >= 2
		self.xlim = xlim
		self.ylim = ylim
		self.num = num

	@property
	def xmin(self) -> float:
		return self.xlim[0]

	@property
	def xmax(self) -> float:
		return self.xlim[1]

	@property
	def ymin(self) -> float:
		return self.ylim[0]

	@property
	def ymax(self) -> float:
		return self.ylim[1]

	@property
	def xnum(self) -> int:
		return self.num[0]

	@property
	def ynum(self) -> int:
		return self.num[1]

	@property
	def xstep(self) -> float:
		return (self.xmax - self.xmin) / float(self.xnum - 1)

	@property
	def ystep(self) -> float:
		return (self.ymax - self.ymin) / float(self.ynum - 1)

	@property
	def points(self):
		raise NotImplementedError("Grid class has no 'points' property")


class SquareGrid(Grid):
	def __init__(self, xlim: np.ndarray, ylim: np.ndarray, res: np.ndarray):
		super().__init__(xlim, ylim, res)
		self.grid = np.zeros((self.xnum, self.ynum, 2))
		for j in range(self.ynum):
			for i in range(self.xnum):
				self.grid[i, j, 0] = self.xmin + self.xstep * i
				self.grid[i, j, 1] = self.ymin + self.ystep * j

	def __getitem__(self, item):
		return self.grid[item]

	@property
	def points(self):
		return self.grid.reshape((-1, 2))


class HexGridV(Grid):
	def __init__(self, xlim: np.ndarray, ylim: np.ndarray, res: np.ndarray):
		super().__init__(xlim, ylim, res)
		self.grid_points = []
		for i in range(self.ynum):
			for j in range(self.xnum):
				if i % 2 == 0:		# EVEN ROW
					if (j+1) % 3 != 0:
						self.grid_points.append(np.array(
							[self.xmin + self.xstep * i, self.ymin + self.ystep * j]))
				else: 				# EVEN COLUMN
					if j % 3 != 0:
						self.grid_points.append(np.array(
							[self.xmin + self.xstep * i, self.ymin + self.ystep * (j + 0.5)]))

	@property
	def points(self):
		return np.asarray(self.grid_points)


class HexGridH(Grid):
	def __init__(self, xlim: np.ndarray, ylim: np.ndarray, res: np.ndarray):
		super().__init__(xlim, ylim, res)
		self.grid_points = []
		for i in range(self.ynum):
			for j in range(self.xnum):
				if i % 2 == 0:		# EVEN ROW
					if (j+1) % 3 != 0:
						self.grid_points.append(np.array(
							[self.xmin + self.xstep * j, self.ymin + self.ystep * i]))
				else: 				# EVEN COLUMN
					if j % 3 != 0:
						self.grid_points.append(np.array(
							[self.xmin + self.xstep * (j + 0.5), self.ymin + self.ystep * i]))

	@property
	def points(self):
		return np.asarray(self.grid_points)
