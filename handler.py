"""
HANDLER.PY:
	handler class for interactivity with the plot

Authors: Chris Janaqi, Alexandre Frances
Created: 25.11.2019
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from matplotlib.backend_bases import MouseEvent, KeyEvent, CloseEvent, ResizeEvent
from tkinter.messagebox import askokcancel
from tkinter.filedialog import asksaveasfilename
import tkinter as tk
import tkinter.ttk as ttk

from model import Model


class Handler:
	"""
	Handler class that handles all events for a plot
	"""
	def __init__(self):
		subplot = plt.subplots()
		self.fig = subplot[0]
		self.ax = subplot[1]
		self.fig.tight_layout()
		self.ax.set_aspect("equal")
		# listen to events
		self.clickid = self.fig.canvas.mpl_connect('button_press_event', self.on_mouse_click)
		self.scrollid = self.fig.canvas.mpl_connect('scroll_event', self.on_mouse_scroll)
		self.pressid = self.fig.canvas.mpl_connect('key_press_event', self.on_key_press)
		self.closeid = self.fig.canvas.mpl_connect('close_event', self.on_close)
		self.resizeid = self.fig.canvas.mpl_connect('resize_event', self.on_resize)
		self.moveid = None
		self.releaseid = None
		self.replot()

	def on_mouse_click(self, event: MouseEvent):
		# disconect from click event
		self.fig.canvas.mpl_disconnect(self.clickid)
		self.clickid = None
		# listen to release event
		self.releaseid = self.fig.canvas.mpl_connect('button_release_event', self.on_mouse_release)

		# everything went well so we can listen to move events
		self.moveid = self.fig.canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
		self.replot()

	def on_mouse_move(self, event: MouseEvent):
		self.replot()

	def on_mouse_release(self, event: MouseEvent):
		self.fig.canvas.mpl_disconnect(self.moveid)
		self.fig.canvas.mpl_disconnect(self.releaseid)
		self.moveid = None
		self.releaseid = None

		self.clickid = self.fig.canvas.mpl_connect('button_press_event', self.on_mouse_click)
		self.replot()

	def on_mouse_scroll(self, event: MouseEvent):
		"""
		Method called each time the mousewheel event is detected
		:param event: The mouse event
		"""
		pass

	def on_resize(self, event: ResizeEvent):
		pass

	def on_key_press(self, event: KeyEvent):
		self.replot()

	def on_close(self, event: CloseEvent):
		pass

	def close(self):
		plt.close(self.fig)

	def replot(self):
		self.fig.canvas.draw()


class ModelHandler(Handler):
	def __init__(self, model: Model):
		self.model = model
		origin, size = model.bbox
		self.lim = np.max(np.array([origin - size, origin + 2 * size]).T, axis=0)
		super().__init__()

	def on_mouse_click(self, event: MouseEvent):
		if event.button == 1:
			self.on_mouse_left_click(event)
		elif event.button == 3:
			self.on_mouse_right_click(event)

	def on_mouse_left_click(self, event: MouseEvent):
		click = np.array([event.xdata, event.ydata])
		if None in click or not self.model.select(click):
			return
		super().on_mouse_click(event)

	def on_mouse_right_click(self, event: MouseEvent):
		click = np.array([event.xdata, event.ydata])
		if None in click:
			return
		self.model.toggle_anchor(click)
		super().on_mouse_click(event)

	def on_mouse_move(self, event: MouseEvent):
		point = np.array([event.xdata, event.ydata])
		if None in point or event.button != 1:
			return
		self.model.drag(point)
		super().on_mouse_move(event)

	def on_mouse_release(self, event: MouseEvent):
		if event.button == 1:
			self.model.deselect()
		super().on_mouse_release(event)

	def replot(self):
		self.ax.clear()
		self.ax.set_xlim(*self.lim)
		self.ax.set_ylim(*self.lim)
		self.ax.triplot(self.model.triangulation)
		self.ax.scatter(*self.model.surface.T)
		self.ax.scatter(*self.model.center)
		anchors = self.model.anchor_points
		if anchors is not None:
			self.ax.scatter(*anchors.T, s=50, c='r')
		if self.model.selected is not None:
			self.ax.scatter(*self.model.selected_point, s=50)

		super().replot()


class DrawHandler(Handler):
	"""
	Drawer class that handles point creation via user's mouse clicks
	"""
	def __init__(self):
		self.points = np.zeros((0, 2))
		self.idx = None
		self.root = tk.Tk()
		s = ttk.Style()
		s.theme_use('classic')
		self.root.withdraw()
		super().__init__()

	def on_mouse_click(self, event: MouseEvent):
		click = np.array([event.xdata, event.ydata])
		if None in click:
			return
		if self.points.shape[0] == 0:
			self.points = np.append(self.points, [click], axis=0)
			super().on_mouse_click(event)
			return
		dist = np.linalg.norm(self.points - click, axis=1)
		index = np.argmin(dist)
		if dist[index] < 0.2:
			# the user clicked an existing point on the plot
			self.idx = index
			if index == 0:
				# the user closed the curve
				self.save()
		else:
			self.points = np.append(self.points, [click], axis=0)
		super().on_mouse_click(event)

	def on_mouse_move(self, event: MouseEvent):
		if self.idx is not None:
			click = np.array([event.xdata, event.ydata])
			self.points[self.idx] = click
		super().on_mouse_move(event)

	def on_mouse_release(self, event: MouseEvent):
		self.idx = None
		super().on_mouse_release(event)

	def on_key_press(self, event: KeyEvent):
		super().on_key_press(event)

	def replot(self):
		self.ax.clear()
		self.ax.set_title("Click on the canvas to draw your shape")
		self.ax.set_xlim(0, 10)
		self.ax.set_ylim(0, 10)
		self.ax.scatter(*self.points.T)
		self.ax.plot(*self.points.T)
		self.fig.canvas.draw()

	def save(self):
		"""
		save the contour drawn by the user
		"""
		yes = askokcancel("Model Creator", "Would you like to save this model?", parent=self.root)
		if yes:
			filename = asksaveasfilename(parent=self.root)
			self.root.destroy()
			if filename:
				np.savetxt(filename, self.points)
				self.close()
				return
		self.root = tk.Tk()
		self.root.withdraw()
