
import numpy as np
import matplotlib.tri as mpltri
from typing import Optional, Union, Iterable, List


class Triangle:
	"""
	Defines a triangle
	"""
	def __init__(self, vertices: np.ndarray, neighbors: np.ndarray):
		"""
		:param vertices: array of indices of points
		:param neighbors: array of indices of neighbors (referring to triangles)
		"""
		self.vertices = vertices
		self.neighbors = neighbors
		self.edges = [
			Edge(self.vertices[0], self.vertices[1], self.neighbors[0]),
			Edge(self.vertices[1], self.vertices[2], self.neighbors[1]),
			Edge(self.vertices[2], self.vertices[0], self.neighbors[2])
		]
		self.half_edges = [
			HalfEdge(self.vertices[0], self.vertices[1], self.vertices[2], self),
			HalfEdge(self.vertices[1], self.vertices[2], self.vertices[0], self),
			HalfEdge(self.vertices[2], self.vertices[0], self.vertices[1], self)
		]


class Edge:
	"""
	Defines an edge
	"""
	def __init__(self, origin: int, end: int, neighbor: int):
		"""
		:param neighbor: an int referring to the index of the triangle
		"""
		self.origin = origin
		self.end = end
		self.neighbor = neighbor


class HalfEdge:
	"""
	Kinda defines a half-edge structure
	"""
	def __init__(self, origin: int, end: int, neighbor_point: int, triangle: Triangle):
		self.origin = origin
		self.end = end
		self.pair = None
		self.triangle = triangle
		self.neighbor_point = neighbor_point
		self.treated = False


class EdgeData:
	"""
	Defines an edge as required in the algorithm
	"""
	def __init__(self, origin: int, end: int, left_neigh: int, right_neigh: int):
		"""
		All parameters are indices indicating the points concerned
		"""
		self.origin = origin
		self.end = end
		self.l_neighbor = left_neigh
		self.r_neighbor = right_neigh


class TriangulationData:
	"""
	Contains the data obtained via mpltri.Triangulation
	"""
	def __init__(self, tri: mpltri.Triangulation):
		self.tri_edges = tri.edges
		self.tri_triangles = tri.triangles
		self.tri_neighbors = tri.neighbors
		self.tri_nedges = len(tri.edges)
		self.triangles = None
		self.edges = None
		self.half_edges = []

	def find_pair_half_edge(self, triangle: Triangle, edge: "Edge"):
		tri = edge.neighbor
		if tri == -1:
			return None
		for i, e in enumerate(self.triangles[tri].edges):
			if e.neighbor == triangle:
				return self.triangles[tri].half_edges[i]

	def _build_triangles(self):
		# tri_neighbors[i] gives the indices of the 3 surrounding triangles (1 per edge)
		self.triangles = [Triangle(vertices, self.tri_neighbors[i]) for i, vertices in enumerate(self.tri_triangles)]
		for i, triangle in enumerate(self.triangles):
			triangle.half_edges[0].pair = self.find_pair_half_edge(i, triangle.edges[0])
			triangle.half_edges[1].pair = self.find_pair_half_edge(i, triangle.edges[1])
			triangle.half_edges[2].pair = self.find_pair_half_edge(i, triangle.edges[2])
			self.half_edges.append(triangle.half_edges[0])
			self.half_edges.append(triangle.half_edges[1])
			self.half_edges.append(triangle.half_edges[2])

	def _build_neighbor(self):
		neighbors = np.zeros((self.tri_nedges, 4), dtype=int)
		i = 0
		for half_edge in self.half_edges:
			if half_edge.pair:
				if not half_edge.pair.treated:
					neighbors[i, :] = np.array([
						half_edge.origin, half_edge.end,
						half_edge.neighbor_point, half_edge.pair.neighbor_point
					])
					half_edge.treated = True
					i += 1
			else:
				neighbors[i, :] = np.array([
					half_edge.origin, half_edge.end,
					half_edge.neighbor_point, -1
				])
				half_edge.treated = True
				i += 1
		return neighbors

	def build_data(self):
		self._build_triangles()
		return self._build_neighbor()

