#!/usr/bin/env python3
"""
CREATOR.PY:
	Contour creator allows the user to draw a contour using the mouse

Authors: Chris Janaqi, Alexandre Frances
Created: 13.01.2020
"""
import matplotlib.pyplot as plt

from handler import DrawHandler


def main():
	handler = DrawHandler()
	plt.show()


if __name__ == '__main__':
	main()
