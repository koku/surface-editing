"""
TRIANGULATION.PY:
	Contains classes to build half-edge structure

Authors: Chris Janaqi, Alexandre Frances
Created: 23.12.2019
"""

import numpy as np
import matplotlib.tri as mpltri
from typing import Optional, Union, Iterable, List


class Triangle:
	"""
	Defines a triangle
	"""
	def __init__(self, vertices: np.ndarray):
		self.vertices = vertices
		self.edges = []
		self.half_edges = []

	def complete_edges(self, neighbors: List["Triangle"]):
		self.edges.append(Edge(self.vertices[0], self.vertices[1], neighbors[0]))
		self.edges.append(Edge(self.vertices[1], self.vertices[2], neighbors[1]))
		self.edges.append(Edge(self.vertices[2], self.vertices[0], neighbors[2]))

	def set_half_edges(self):
		self.half_edges.append(HalfEdge(self.vertices[0], self.vertices[2], self))
		self.half_edges.append(HalfEdge(self.vertices[1], self.vertices[0], self))
		self.half_edges.append(HalfEdge(self.vertices[2], self.vertices[1], self))

	def find_pair_half_edge(self, edge: "Edge"):
		tri = edge.neighbor
		if tri is None:
			return None
		for i, e in enumerate(tri.edges):
			if e.neighbor == self:
				return tri.half_edges[i]

	def complete_half_edges(self):
		self.half_edges[0].next_he = self.half_edges[1]
		self.half_edges[1].next_he = self.half_edges[2]
		self.half_edges[2].next_he = self.half_edges[0]

		self.half_edges[0].prev_he = self.half_edges[2]
		self.half_edges[1].prev_he = self.half_edges[0]
		self.half_edges[2].prev_he = self.half_edges[1]

		self.half_edges[0].pair = self.find_pair_half_edge(self.edges[0])
		self.half_edges[1].pair = self.find_pair_half_edge(self.edges[1])
		self.half_edges[2].pair = self.find_pair_half_edge(self.edges[2])


class Edge:
	"""
	Defines an edge
	"""
	def __init__(self, p1: np.ndarray, p2: np.ndarray, neighbor: Triangle):
		self.origin = p1
		self.end = p2
		self.neighbor = neighbor


class RealEdge:
	"""
	Defines an edge as needed in the algortighms
	"""
	def __init__(self, p1: int, p2: int, left_neighbor: Triangle, right_neighbor: Triangle):
		self.origin = p1		# The indices of point in self.vertices
		self.end = p2
		self.l_neigh = left_neighbor
		self.r_neigh = right_neighbor


class TriangulationData:
	def __init__(self, tri: mpltri.Triangulation):
		self.x, self.y = tri.x, tri.y
		self.triangles = tri.triangles
		self.edges = tri.edges
		self.neighbors = tri.neighbors
		self.nedges = len(tri.edges)

		self.vertices = np.concatenate((np.array([self.x]), np.array([self.y])), axis=0).T
		self.real_triangles = None
		self.half_edges = None
		self.final_edges = None

	def _build_triangles(self):
		self.real_triangles = [Triangle(a) for a in self.triangles]
		for i, triangle in enumerate(self.real_triangles):
			neighbors = [
				self.real_triangles[self.neighbors[i, 0]] if self.neighbors[i, 0] != -1 else None,
				self.real_triangles[self.neighbors[i, 1]] if self.neighbors[i, 1] != -1 else None,
				self.real_triangles[self.neighbors[i, 2]] if self.neighbors[i, 2] != -1 else None
			]
			triangle.complete_edges(neighbors)

	def _build_half_edges(self):
		self.half_edges = []
		for triangle in self.real_triangles:
			triangle.set_half_edges()

		for triangle in self.real_triangles:
			triangle.complete_half_edges()
			self.half_edges.append(triangle.half_edges[0])
			self.half_edges.append(triangle.half_edges[1])
			self.half_edges.append(triangle.half_edges[2])

	def _build_real_edges(self):
		self.final_edges = []
		for half_edge in self.half_edges:
			if half_edge.pair:
				if not half_edge.pair.treated:
					self.final_edges.append(
						RealEdge(
							half_edge.origin, half_edge.next_he.origin,
							half_edge.neighbor_point, half_edge.pair.neighbor_point
						)
					)
					half_edge.treated = True
			else:
				self.final_edges.append(
					RealEdge(
						half_edge.origin, half_edge.next_he.origin,
						half_edge.neighbor_point, -1
					)
				)
				half_edge.treated = True

	def build_data(self):
		neighbors = np.zeros((self.nedges, 4), dtype=int)
		self._build_triangles()
		self._build_half_edges()
		self._build_real_edges()
		for i, edge in enumerate(self.final_edges):
			neighbors[i] = np.array([edge.origin, edge.end, edge.l_neigh, edge.r_neigh])
		return neighbors, self.vertices


class HalfEdge:
	def __init__(self, origin: int, neighbor_point: int, triangle: Triangle):
		self.origin = origin		# Origin of the half-edge
		self.pair = None 		# Opposite half-edge (belonging to a neighbor triangle)
		self.triangle = triangle  	# The triangle the half-edge is belonging to
		self.neighbor_point = neighbor_point 	# The point of the triangle that is not the origin nor the end
		self.next_he = None 	# The next half-edge
		self.prev_he = None 	# The previous half-edge
		self.treated = False

