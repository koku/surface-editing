#!/usr/bin/env python3
"""
MAIN.PY:
	Descrpition

Authors: Chris Janaqi, Alexandre Frances
Created: 25.11.2019
"""
import numpy as np
import matplotlib.pyplot as plt
from typing import Optional
import sys

from model import Model
from handler import ModelHandler, DrawHandler


def load_model(filename: str, grid_type: Optional[str] = 'square') -> Model:
	points = np.loadtxt(filename)
	return Model(points, grid_type)


def main():
	narg = len(sys.argv)
	if narg <= 1:
		model = load_model("models/tibiscuit.txt", 'hexH')
	elif narg == 2:
		model = load_model(sys.argv[1])
	else:
		model = load_model(sys.argv[1], sys.argv[2])
	# model = Model(np.array([[0., 0.], [0., 1.], [1., 1.], [1., 0.]]), None)
	handler = ModelHandler(model)
	plt.show()


if __name__ == '__main__':
	main()
