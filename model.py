"""
MODEL.PY:
	Model class that represent a 2d model to edit

Authors: Chris Janaqi, Alexandre Frances
Created: 25.11.2019
"""
import numpy as np
import matplotlib.tri as mpltri
from typing import Optional, Union, Iterable

from grid import SquareGrid, HexGridV, HexGridH
from triangulation import TriangulationData


class Model:
	def __init__(self, points: np.ndarray, use_grid: Optional[str] = 'square', anchors: Optional[Iterable[int]] = None):
		"""
		Create a new Model
		:param points: vector of point of shape (npoints, 2)
		:param use_grid: use the grid
		:param anchors: points of the model to keep static
		"""
		_, dim = points.shape
		assert dim == 2
		# Compute Axis Aligned Bounding Box
		origin = np.min(points, axis=0)
		self.bbox = np.array([origin, np.max(points, axis=0) - origin])

		# Create contour and surface
		self.contour = points
		self.surface = self._create_surface(use_grid) if use_grid else self.contour

		# Create triangulation
		tri = mpltri.Triangulation(*self.surface.T)
		centers = np.mean(self.surface[tri.triangles], axis=1)
		mask = self.contains(centers)
		tri = mpltri.Triangulation(*self.surface.T, tri.triangles[mask])

		# TEST DE LA PRESENTATION DES STRUCTURES HALF EDGE
		tri2 = TriangulationData(tri)  # TEST
		self.neighbors = tri2.build_data()
		# DEBUGUER LA LIGNE 44 POUR VOIR LES DONNEES DES HALF EDGES (
		self.triangles = tri.triangles

		self.anchors = list(anchors) if anchors else []
		self.selected = None

		self.deformation = Deformation(self)

	@property
	def ntri(self) -> int:
		return self.triangles.shape[0]

	@property
	def nedges(self) -> int:
		return self.neighbors.shape[0]

	@property
	def edges(self) -> np.ndarray:
		return self.neighbors[:, :2]

	@property
	def npoints(self) -> int:
		return self.surface.shape[0]

	@property
	def nconstrains(self) -> int:
		return len(self.anchors)

	@property
	def selected_point(self) -> Optional[np.ndarray]:
		return self.surface[self.selected] if self.selected is not None else None

	@property
	def anchor_points(self) -> Optional[np.ndarray]:
		return self.surface[self.anchors] if self.anchors is not None else None

	@property
	def triangulation(self) -> mpltri.Triangulation:
		return mpltri.Triangulation(*self.surface.T, self.triangles)

	@property
	def center(self) -> np.ndarray:
		centers = np.mean(self.surface[self.triangles], axis=1)
		return np.mean(centers, axis=0)

	def _create_surface(self, use_grid: str) -> np.ndarray:
		origin, size = self.bbox
		xlim, ylim = np.array([origin, origin + size]).T
		epsilon = 0.03 * max(xlim[1] - xlim[0], ylim[1] - ylim[0])  # CHANGE HERE IF YOU WANT !!
		if use_grid == 'square':
			grid = SquareGrid(xlim, ylim, np.array([10, 10]))
		elif use_grid == 'hexH':
			grid = HexGridH(xlim, ylim, np.array([15, 15]))
		elif use_grid == 'hexV':
			grid = HexGridV(xlim, ylim, np.array([15, 15]))
		else:
			raise KeyError("Grid name unvalid. Use 'square', 'hexH' or 'hexV'.")
		grid_points = grid.points
		grid_mask_contains = self.contains(grid_points)
		grid_mask_close = self.close(grid_points, epsilon)
		to_keep = grid_mask_contains & ~grid_mask_close
		points = grid_points[to_keep]
		surface = np.asarray(points)
		return np.concatenate((self.contour, surface))

	def contains(self, points: np.ndarray) -> np.ndarray:
		"""
		Test if a list of points are inside the model contour
		:param points: array of points of shape (npoints, dim)
		:return: array of bool of shape (npoints,) - True if inside, False if not
		"""
		npoints, dim = points.shape
		if dim != 2:
			raise ValueError("Model can only contain 2D points")
		res = np.zeros(npoints, dtype=np.bool)
		polyline = np.append(self.contour, [self.contour[0]], axis=0)
		for i, point in enumerate(points):
			pts_relative = polyline - point
			thetas = np.arctan2(pts_relative[:, 1], pts_relative[:, 0])
			theta_diff = np.diff(thetas)
			theta_diff = np.where(theta_diff > np.pi, theta_diff - 2 * np.pi, theta_diff)
			theta_diff = np.where(theta_diff < -np.pi, theta_diff + 2 * np.pi, theta_diff)
			theta_sum = np.abs(np.sum(theta_diff))
			res[i] = np.round(theta_sum / (2 * np.pi)) != 0
		return res

	def close(self, points: np.ndarray, epsilon: Optional[float] = None) -> np.ndarray:
		"""
		Test if a list of points are too close of the contour defined by points
		:param epsilon: the arbitrary distance to choose if the point is to close or not
		:param points: array of points of shape (npoints, dim)
		:return: array of bool of shape (npoints,) - True if close, False if not
		"""
		# TODO: Optimize this without for loop
		npoints, dim = points.shape
		if dim != 2:
			raise ValueError("Model can only contain 2D points")
		if epsilon is None:
			epsilon = 0.1
		res = np.zeros(npoints, dtype=np.bool)
		for i, point in enumerate(points):
			distance = np.linalg.norm(point - self.contour, axis=1)
			distance = np.where(distance < epsilon, True, False)
			res[i] = np.any(distance)
		return res

	def pick(self, point: np.ndarray) -> Optional[int]:
		_, size = self.bbox
		dist = np.linalg.norm((self.surface - point) / size, axis=1)
		index = np.argmin(dist)
		if dist[index] > 0.05:
			return None
		return index

	def select(self, point: np.ndarray) -> bool:
		index = self.pick(point)
		if index is None:
			return False
		# print("Selected point {}".format(index))
		if index not in self.anchors:
			self.anchors.append(index)
		self.selected = index
		self.deformation.update_constrains()
		return True

	def deselect(self):
		self.anchors.remove(self.selected)
		self.deformation.update_points()
		self.selected = None

	def toggle_anchor(self, point: np.ndarray):
		index = self.pick(point)
		if index is None:
			return
		if index in self.anchors:
			self.anchors.remove(index)
		else:
			self.anchors.append(index)

	def drag(self, point: np.ndarray):
		self.deformation.on_drag(point)

	def rotate(self, matrix: np.ndarray):
		center = self.center
		self.contour -= center
		self.contour = self.contour @ matrix.T
		self.contour += center
		self.surface -= center
		self.surface = self.surface @ matrix.T
		self.surface += center

	def translate(self, vec: np.ndarray):
		self.contour += vec
		self.surface += vec


D = np.array([
	[-1, 0, 1, 0, 0, 0, 0, 0],
	[0, -1, 0, 1, 0, 0, 0, 0],
])

w = 1000


class Deformation:
	def __init__(self, model: Model):
		self.model = model
		self.n, self.m = self.model.npoints, self.model.nedges
		self.drag_func = self.simple_drag
		self.A1 = np.zeros((2 * self.m, 2 * self.n))
		self.b1 = None
		self.update_points()
		self.update_constrains()

	def update_constrains(self):
		n, m, K = self.model.npoints, self.model.nedges, self.model.nconstrains
		anchors = self.model.anchors
		if self.model.nconstrains <= 1:
			# print("Drag func: simple")
			self.drag_func = self.simple_drag
		else:
			# print("Drag func: arap")
			self.drag_func = self.arap_drag
			A1 = self.A1
			self.A1 = np.zeros((2 * m + 2 * K, 2 * n))
			self.A1[:2 * m, :] = A1[:2 * m, :]
			C1 = np.zeros((2 * K, 2 * n))
			# construction of C1
			for k, anchor in enumerate(anchors):
				C1[2 * k, 2 * anchor] = w
				C1[2 * k + 1, 2 * anchor + 1] = w
			self.A1[2 * m:, :] = C1
		self.b1 = np.zeros((2 * m + 2 * K,))
		self.b1[2 * m:] = w * self.model.anchor_points.flatten()

	def update_points(self):
		n, m = self.model.npoints, self.model.nedges
		points = self.model.surface
		points2 = np.array([points[:, 1], -points[:, 0]]).T

		edges = points[self.model.edges[:, 1]] - points[self.model.edges[:, 0]]
		edges2 = np.array([edges[:, 1], -edges[:, 0]]).T

		neighbors = self.model.neighbors
		left = neighbors[:, 2] != -1
		right = neighbors[:, 3] != -1

		G = np.zeros((m, 8, 2))
		E = np.zeros((m, 2, 2))

		# construction of G
		G[:, [0, 2], :] = points[neighbors[:, 0:2], :]
		G[:, [1, 3], :] = points2[neighbors[:, 0:2], :]
		G[left, 4, :] = points[neighbors[left, 2], :]
		G[left, 5, :] = points2[neighbors[left, 2], :]
		G[right, 6, :] = points[neighbors[right, 3], :]
		G[right, 7, :] = points2[neighbors[right, 3], :]
		# construction of E
		E[:, 0, :] = edges
		E[:, 1, :] = edges2
		# construction of H from E and G
		H = [D - E[k] @ np.linalg.inv(G[k].T @ G[k]) @ G[k].T for k in range(m)]
		# construction of L1
		L1 = np.zeros((2 * m, 2 * n))
		for k, h in enumerate(H):
			neighbor = neighbors[k, :]
			neighbor = neighbor[neighbor != -1]

			L1[2 * k:2 * k + 2, 2 * neighbor] = h[:, :2 * len(neighbor):2]
			L1[2 * k:2 * k + 2, 2 * neighbor + 1] = h[:, 1:2 * len(neighbor) + 1:2]

		# updates A1
		self.A1[:2 * m, :] = L1
		# print(self.A1)

	def on_drag(self, new_pos: np.ndarray):
		self.update_lhs(new_pos)
		self.drag_func(new_pos)

	def simple_drag(self, new_pos: np.ndarray):
		"""
		Simple drag model in case where only 1 anchor has been set
		translate and rotate the whole model around its center
		"""
		assert self.model.selected is not None
		selected_point = self.model.selected_point
		center = self.model.center
		delta = new_pos - selected_point
		radius = np.linalg.norm(selected_point - center)
		normal = normalize(selected_point - center)
		tangent = orthogonal(normal)
		translation = np.dot(delta, normal)
		rotation = np.dot(delta, tangent)

		hypotenuse = np.sqrt(radius ** 2 + rotation ** 2)
		cos = radius / hypotenuse
		sin = rotation / hypotenuse
		matrix = np.array([
			[cos, -sin],
			[sin, cos],
		])
		self.model.rotate(matrix)
		self.model.translate(translation * normal)

	def arap_drag(self, _: np.ndarray):
		"""
		As rigid as possible manipulation:
			Implementation of the igarashi's deformation formula
		"""
		assert self.A1 is not None
		assert self.b1 is not None
		ATA = self.A1.T @ self.A1
		ATb1 = self.A1.T @ self.b1
		points = np.linalg.solve(ATA, ATb1)
		points = points.reshape((-1, 2))
		self.model.surface = points

	def update_lhs(self, new_pos):
		index = self.model.anchors.index(self.model.selected)
		constrain = 2*self.m + 2*index
		self.b1[constrain:constrain+2] = w * new_pos


def normalize(vec: np.ndarray) -> np.ndarray:
	"""
	Normalize a vector handling 0
	:param vec: input vector
	:return:
	"""
	length = np.linalg.norm(vec)
	return vec / length if length != 0 else vec


ROTATION_90 = np.array([
	[0, -1],
	[1, 0],
])


def orthogonal(vec: np.ndarray) -> np.ndarray:
	"""
	Rotate a vector by an angle
	:param vec: input vector of shape (n, 2)
	:return: rotated vector
	"""
	if vec.shape == (2,):
		return ROTATION_90 @ vec
	return (ROTATION_90 @ vec.T).T
